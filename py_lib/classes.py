"""This module is used for useful base classes to inherit from"""

from __future__ import unicode_literals

try:
    from umsgpack import packb as _packb, unpackb as _unpackb
except ImportError:
    from msgpack import packb as _packb, unpackb as _unpackb

try:
    from .py3_only.classes import Singleton
except SyntaxError:
    from .py2_only.classes import Singleton

from warnings import warn


def packb(an_object):
    """A wrapper around the standard msgpack packer to enable length headers"""
    buff = _packb(an_object)
    return _packb(len(buff)) + buff


def unpackb(buff, header):
    """A wrapper around the standard msgpack reader to enable length headers"""
    if header:
        _, buff = strip_length(buff)
    return _unpackb(buff)


def strip_length(buff):
    if buff[:1] < b'\xf0':
        return _unpackb(buff[:1]), buff[1:]
    elif buff[:1] in (b'\xcc', b'\xd0'):
        return _unpackb(buff[:2]), buff[2:]
    elif buff[:1] in (b'\xcd', b'\xd1'):
        return _unpackb(buff[:3]), buff[3:]
    elif buff[:1] in (b'\xce', b'\xd2'):
        return _unpackb(buff[:5]), buff[5:]
    elif buff[:1] in (b'\xcf', b'\xd3'):
        return _unpackb(buff[:9]), buff[9:]


class Serializable(object):
    """This class is inherited from as a shorthand to make a class serializable.
    The way you do this is by providing an subclass.serialized() method for it
    to build on. You can use *positional* arguments in any way that is also
    valid for subclass.__init__(). An example would look like:

    class A(Serializable):
        def __init__(self, a, b):
            self.a = a
            self.b = b

        def serialized(self):
            return super(A, self).serialized(self.a, self.b)

    It can also do limited inferences as to what gets serialized. This breaks,
    however, if you inherit from more than just Serialized. So the above could
    be simplified to:

    class A(Serializable):
        __slots__ = ('a', 'b')

        def __init__(self, a, b):
            self.a = a
            self.b = b

    Raises:
        - NotImplementedError if a subclass did not tell it what to encode
        - whatever msgpack.packb raises if you give it unserializable items"""

    def serialized(self, *args):
        """This is the boilerplate serialization method"""
        name = self.__class__.__name__
        if not args:
            warn("No args given. Attempting to use __slots__. This will not "
                 "work if ordered improperly.")
            if getattr(self, '__slots__', ()):
                args = [getattr(self, k) for k in self.__slots__]
            if not args or len(type(self).mro()) > 3:
                raise NotImplementedError(
                    "No {}.serialized() to build from".format(name))
        return packb([name] + list(args))

    @classmethod
    def deserialize(cls, buffer, header=True):
        """This is the boilerplate deserialization method. It takes in a
        msgpack buffer, and decodes it into a tuple of (classname, args,
        kwargs). This is then fed back into the subclass.recombine() method.
        Extending this particular implementation is not really feasible in most
        instances, so it is recommended that you modify subclass.serialized()
        and subclass.recombine() instead.

        Raises:
            - ValueError if fed a serialized instance of the wrong class
            - whatever msgpack.unpackb raises when given a bad buffer"""
        args = unpackb(buffer, header)
        if args[0] != cls.__name__:
            raise ValueError(
                "This is not a serialized instance of the right class")
        return cls.recombine(*args[1:])

    @classmethod
    def recombine(cls, *args):
        """This is the boilerplate recombination method. It takes in the
        result of subclass.deserialize() and transforms it into an instance of
        subclass. By default it takes the same arguments as
        subclass.__init__()"""
        return cls(*args)


class SignedSerializable(Serializable):
    """This subclass of Serializable includes a signature and public key for
    the deserializer to verify. The only main difference is that when extending
    subclass.serialized() you *must* include a positional argument "key" after
    "self".

    Note: this will not yet work, because there is currently no Key abstraction
    class to use."""

    def serialized(self, key, *args):
        buff = super(SignedSerializable, self).serialized(*args)
        return buff + packb(key.sign(buff)) + key.serialized()

    @classmethod
    def deserialize(cls, buff, header=True):
        if not header:
            raise ValueError("You need a length header to deserailze")
        length, buff = strip_length(buff)
        buff, sig_data = buff[:length], buff[length:]
        length, sig_data = strip_length(sig_data)
        sig_data, raw_key = _unpackb(sig_data[:length]), sig_data[length:]
        key = KeyClass.deserialize(raw_key)
        key.validate(buff, sig)
        return super(SignedSerializable, cls).deserialize(buff, header=False)

    def __init__(self):
        raise NotImplementedError()


class EncryptedSerializable(Serializable):
    def __init__(self):
        raise NotImplementedError()


class SignedEncryptedSerializable(SignedSerializable, EncryptedSerializable):
    def __init__(self):
        raise NotImplementedError()
