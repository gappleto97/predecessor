from ..metaclasses.singletonmeta import SingletonMetaClass


class Singleton(object, metaclass=SingletonMetaClass):
    """This class is inherited from whenever you want to have only a single
    instance of a class. It is implemented using a metaclass, so the python2
    and python3 stubs are different, though they share the same underlying
    code"""
