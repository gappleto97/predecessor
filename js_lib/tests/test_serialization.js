const expect = require('expect');
const Serializable = require('../index.js').Serializable;

describe('The Serializable base class:', () => {
    it('will properly deserialize', () => {
        class Test extends Serializable {
            constructor(a) {
                super();
                this.a = a;
            }

            serialized() {
                return super.serialized(this.a);
            }
        }
        let a = new Test(1);
        let b = Test.deserialize(a.serialized());
        expect(b.a).toEqual(a.a);
        expect(a === b).toEqual(false);
    });

    it('should properly deserialize implied classes', () => {
        class Test extends Serializable {
            constructor(a, b)   {
                super();
                this._slots = ['a', 'b'];
                this.a = a;
                this.b = b;
            }
        }
    });

    it('should fail to deserialize empty classes', () => {
        class Test extends Serializable {}
        let a = new Test();
        try {
            let b = Test.deserialize(a.serialized());
        } catch (e) {
            a.failed = true;
        }
        if (!a.failed) {
            throw new Error("This should come up if it successfully deserialized");
        }
    });

    it('should properly deserialize predefined test cases', () => {
        class Sample extends Serializable {
            constructor(a, b) {
                super();
                this.a = a;
                this.b = b;
            }

            serialized() {
                return super.serialized(this.a, this.b);
            }
        }
        let buff = require('fs').readFileSync('./build/test_case.object');
        expect(Sample.deserialize(buff)).toBeTruthy();
    });
});
